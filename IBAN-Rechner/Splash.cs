﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			Splash.cs
Einsatz:		Splash-Screen
Beschreibung:	Zeigt einen Splash-Screen
Funktionen:		Splash
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static void Splash()
        {
            Console.WriteLine("Programm: IBAN-Rechner und Validator Version 1.0");
            Console.WriteLine("Autor: Jean Luc Nürrenberg");
            Console.WriteLine("Datum: 08/2018");
            Console.WriteLine("Beschreibung: Berechnet und validiert IBANs");
        }
    }
}
