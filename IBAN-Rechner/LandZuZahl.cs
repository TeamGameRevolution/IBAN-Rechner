﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			LandZuZahl.cs
Einsatz:		Konvertiert ein Länderkürzel zu seiner notwendigen Zahl
Beschreibung:	Das Länderkürzel muss zu einer Zahl konvertiert werden, wo A=10, B=11 usw.
Funktionen:		LandZuZahl
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static string LandZuZahl(string land)
        {
            string ausgabe = "";
            for (int i = 0; i < land.Length; i++)
            {
                ausgabe = ausgabe + (Convert.ToInt32(land[i]) - 55);
            }

            return ausgabe;
        }
    }
}
