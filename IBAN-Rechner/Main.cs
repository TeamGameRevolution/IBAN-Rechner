﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			main.cs
Einsatz:		Enthält die Logik zum Ausführen des Programms
Beschreibung:	Berechnet und validiert IBANs
Funktionen:		Main
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static void Main(string[] args)
        {
            IBANFormat[] formate = new IBANFormat[0];
            IBANDateiEinlesen("ibanFormate.csv", ref formate);
            DeutscheBankleitzahl[] bankleitzahlen = new DeutscheBankleitzahl[0];
            BankleitzahlenDateiEinlesen("DESWIFT.csv", ref bankleitzahlen);
            Run(formate, bankleitzahlen);
        }
    }
}
