﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			IBANBerechnen.cs
Einsatz:		Berechnen einer IBAN aus Kontonummer, Bankleitzahl und Ländercode
Beschreibung:	Berechnet die Prüfnummer anhand der Kontonummer, Bankleitzahl und Länderkennung und baut damit eine valide IBAN auf
Funktionen:		BerechneIBAN
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static string BerechneIBAN(string bban, string laenderkennung)
        {
            string pruefnummer = PruefnummerBerechnen(bban, laenderkennung);

            string iban = laenderkennung + pruefnummer + bban;
            return iban;
        }
    }
}
