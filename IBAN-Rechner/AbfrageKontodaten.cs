﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static void AbfrageKontodaten(string land, IBANFormat[] formate, DeutscheBankleitzahl[] bankleitzahlen)
        {
            bool enthaeltLand = false;
            IBANFormat format = new IBANFormat();
            for (int i = 0; i < formate.Length; i++)
            {
                if (formate[i].GetKuerzel() == land)
                {
                    enthaeltLand = true;
                    format = formate[i];
                    break;
                }
            }

            if (!enthaeltLand)
            {
                Console.WriteLine("Das angegebene Land konnte nicht gefunden werden.");
                Console.WriteLine("Drücken Sie Enter zum Fortfahren!");
                Console.ReadLine();
                return;
            }

            string landName = format.GetLand();
            string landKuerzel = format.GetKuerzel();
            string landFormat = format.GetFormat(); ;

            bool enthaeltKontonummer = landFormat.Contains("k");
            bool enthaeltBankleitzahl = landFormat.Contains("b");
            bool enthaeltKontotyp = landFormat.Contains("d");
            bool enthaeltKontrollzeichen = landFormat.Contains("K");
            bool enthaeltRegionalcode = landFormat.Contains("r");
            bool enthaeltFilialnummer = landFormat.Contains("s");
            bool enthaeltSonstige = landFormat.Contains("X");

            string kontonummer = "";
            string bankleitzahl = "";
            string kontotyp = "";
            string regionalcode = "";
            string filialnummer = "";
            string sonstige = "";

            if (enthaeltKontrollzeichen)
            {
                Console.WriteLine("Die Berechnung einer IBAN dieses Formats ist momentan nicht möglich!");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("IBAN-Berechnung für Konto aus " + landName);
            Console.WriteLine();

            if (enthaeltKontonummer)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 'k');
                Console.Write("Kontonummer (oder ähnliches): ");
                kontonummer = Console.ReadLine();
                if (kontonummer.Length > anzahl)
                {
                    Console.WriteLine("Invalide Kontonummer! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (kontonummer.Length < anzahl)
                {
                    kontonummer = NummerAuffuellen(kontonummer, anzahl);
                }
            }

            if (enthaeltBankleitzahl)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 'b');
                Console.Write("Bankleitzahl (oder ähnliches): ");
                bankleitzahl = Console.ReadLine();
                if (bankleitzahl.Length > anzahl)
                {
                    Console.WriteLine("Invalide Bankleitzahl! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (bankleitzahl.Length < anzahl)
                {
                    bankleitzahl = NummerAuffuellen(bankleitzahl, anzahl);
                }
            }

            if (enthaeltKontotyp)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 'd');
                Console.Write("Kontotyp (oder ähnliches): ");
                kontotyp = Console.ReadLine();
                if (kontotyp.Length > anzahl)
                {
                    Console.WriteLine("Invalider Kontotyp! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (kontotyp.Length < anzahl)
                {
                    kontotyp = NummerAuffuellen(kontotyp, anzahl);
                }
            }

            if (enthaeltRegionalcode)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 'r');
                Console.Write("Regionalcode (oder ähnliches): ");
                regionalcode = Console.ReadLine();
                if (regionalcode.Length > anzahl)
                {
                    Console.WriteLine("Invalider Regionalcode! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (regionalcode.Length < anzahl)
                {
                    regionalcode = NummerAuffuellen(regionalcode, anzahl);
                }
            }

            if (enthaeltFilialnummer)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 's');
                Console.Write("Filialnummer (oder ähnliches): ");
                filialnummer = Console.ReadLine();
                if (filialnummer.Length > anzahl)
                {
                    Console.WriteLine("Invalide Filialnummer! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (filialnummer.Length < anzahl)
                {
                    filialnummer = NummerAuffuellen(filialnummer, anzahl);
                }
            }

            if (enthaeltSonstige)
            {
                int anzahl = ZaehleBuchstaben(landFormat, 'X');
                Console.Write("Sonstige Nummern: ");
                sonstige = Console.ReadLine();
                if (sonstige.Length > anzahl)
                {
                    Console.WriteLine("Invalide sonstige Nummern! Drücken Sie Enter zum Fortfahren!");
                    Console.ReadLine();
                    return;
                }

                if (sonstige.Length < anzahl)
                {
                    sonstige = NummerAuffuellen(sonstige, anzahl);
                }
            }

            string minimiert = MinimiereFormat(landFormat);
            string bban = minimiert.Replace("b", bankleitzahl);
            bban = bban.Replace("d", kontotyp);
            bban = bban.Replace("k", kontonummer);
            bban = bban.Replace("r", regionalcode);
            bban = bban.Replace("s", filialnummer);
            bban = bban.Replace("X", sonstige);

            string iban = BerechneIBAN(bban, landKuerzel);

            if(landKuerzel == "DE")
            {
                DeutscheBankleitzahl bankleitzahlInfo = new DeutscheBankleitzahl();
                for(int i = 0; i < bankleitzahlen.Length; i++)
                {
                    if(bankleitzahlen[i].GetBankleitzahl() == bankleitzahl)
                    {
                        bankleitzahlInfo = bankleitzahlen[i];
                    }
                }

                if(bankleitzahlInfo.GetBankleitzahl() != "")
                {
                    Console.WriteLine();
                    Console.WriteLine("Bankinformationen:");
                    Console.WriteLine(bankleitzahlInfo.GetBezeichnung());
                    Console.WriteLine(bankleitzahlInfo.GetPostleitzahl() + " " + bankleitzahlInfo.GetOrt());
                    Console.WriteLine("BIC: " + bankleitzahlInfo.GetBIC());
                }
            }

            Console.WriteLine();
            Console.WriteLine("Ihre IBAN lautet: " + iban);
            Console.WriteLine("Drücken Sie Enter zum Fortfahren!");
            Console.ReadLine();
        }
    }
}
