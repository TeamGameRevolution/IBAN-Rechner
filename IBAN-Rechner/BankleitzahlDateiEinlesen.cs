﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static void BankleitzahlenDateiEinlesen(string pfad, ref DeutscheBankleitzahl[] bankleitzahlen)
        {
            string[] datei = File.ReadAllLines(pfad);
            bankleitzahlen = new DeutscheBankleitzahl[datei.Length];
            for (int i = 0; i < datei.Length; i++)
            {
                string datensatz = datei[i];
                string[] csv = datensatz.Split(';');

                string bankleitzahl = csv[0];
                string bezeichnung = csv[1];
                string postleitzahl = csv[2];
                string ort = csv[3];
                string kurzbezeichnung = csv[4];
                string bic = csv[5];

                DeutscheBankleitzahl blz = new DeutscheBankleitzahl(bankleitzahl, bezeichnung, postleitzahl, ort, kurzbezeichnung, bic);
                bankleitzahlen[i] = blz;
            }
        }
    }
}
