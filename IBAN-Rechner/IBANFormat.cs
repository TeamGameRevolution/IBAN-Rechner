﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        struct IBANFormat
        {
            string land;
            string kuerzel;
            string format;

            public IBANFormat(string land, string kuerzel, string format)
            {
                this.land = land;
                this.kuerzel = kuerzel;
                this.format = format;
            }

            public IBANFormat(IBANFormat format)
            {
                this.land = format.land;
                this.kuerzel = format.kuerzel;
                this.format = format.format;
            }

            public string GetLand()
            {
                return land;
            }

            public string GetKuerzel()
            {
                return kuerzel;
            }

            public string GetFormat()
            {
                return format;
            }
        }
    }
}
