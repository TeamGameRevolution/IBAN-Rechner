﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			BigIntRechnen.cs
Einsatz:		Rechnen mit sehr großen Zahlen
Beschreibung:	Bietet Funktionen, mit welcher große Zahlen berechnet werden können, hier wird nur Modulo benötigt
Funktionen:		DividiereZahl, DividiereZahlen, Modulo
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        // Dividiert eine Zahl durch eine andere Zahl und schreibt das Ergebnis in das angegebene Zahlen-Array an den gegebenen Index
        // Repliziert schriftliche Division
        static int DividiereZahl(int[] zahlen, int index, int divident, int uebertrag, int divisor)
        {
            // Der Divisor darf nicht größer als 1 Milliarde sein (da unsere Zahlen höchstens 999.999.999 groß sind)
            if (divisor >= 1000000000)
            {
                // Fehlerwert
                return -1;
            }

            // Der Übertrag darf auch nicht größer als der Divisor sein (da dann nicht vollständig dividiert wurde)
            if (uebertrag >= divisor)
            {
                return -1;
            }

            // Berechne den Divident anhand des Übertrags und berechnet Quotient und Übertrag
            long neuDivident = divident + 1000000000L * uebertrag;
            long quotient = neuDivident / divisor;
            long uebrig = neuDivident % divisor;

            // Der Quotient darf ebenfalls nicht größer als 1 Milliarde sein
            if (quotient >= 1000000000)
            {
                return -1;
            }

            // Und der Übertrag darf auch hier nicht größer als der Divisor sein
            if (uebrig >= divisor)
            {
                return -1;
            }

            // Schreibe das Ergebnis der Division an die angegebene Stelle und gebe den Übertrag zurück
            zahlen[index] = (int) quotient;
            return (int) uebrig;
        }

        // Dividiert eine große Zahl durch einen "normalen" Divisor
        static int DividiereZahlen(int[] zahlen, int index, int[] divident, int dividentIndex, int divisor)
        {
            // Beginne mit Übertrag 0
            int uebertrag = 0;
            // Schleife durch die Einträge der großen Zahl und erhöhe den Index des Ergebnisses sowie den Index des Dividenden um 1 nach jeder Iteration
            // Bedeutet effektiv, dass wir zum nächsten Block wechseln
            for (; dividentIndex < divident.Length; dividentIndex++, index++)
            {
                // Dividiere die aktuelle Zahl und speichere den Übertrag
                uebertrag = DividiereZahl(zahlen, index, divident[dividentIndex], uebertrag, divisor);
                // Falls der Übertrag ein Fehlerwert ist, gebe einen Fehlerwert zurück
                if (uebertrag == -1)
                {
                    return -1;
                }
            }
            // Gebe den Übertrag zurück
            return uebertrag;
        }

        static int Modulo(int[] zahlen, int divisor)
        {
            // Dividiere die große Zahl durch einen normalen Divisor (97) und gebe den Übertrag (Rest) zurück
            int[] ausgabe = new int[zahlen.Length];
            return DividiereZahlen(ausgabe, 0, zahlen, 0, divisor);
        }
    }
}
