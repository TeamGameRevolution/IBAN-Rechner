﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			BigIntKonvertierung.cs
Einsatz:		Konvertieren einer sehr großen Zahl (mehr als 24 Stellen) in ein Array von Teilen
Beschreibung:	Konvertiert die große Zahl in eine geteilte Version (100100100244602111131400 -> 100100, 100244602, 111131400)
Funktionen:		StringZuBigIntArray
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static int[] StringZuBigIntArray(string zahl)
        {
            int zahlLaenge = zahl.Length;
            // Praktisch: Math.Ceil() für Int
            int bigLaenge = (zahlLaenge - 1) / 9 + 1;

            int ersteZahlen = zahlLaenge - (bigLaenge - 1) * 9;
            int[] zahlen = new int[bigLaenge];
            for (int i = 0; i < bigLaenge; i++)
            {
                // Den Zahlenblock abrufen
                int start = Max(ersteZahlen + (i - 1) * 9, 0);
                int end = ersteZahlen + i * 9;
                string block = zahl.Substring(start, end - start);
                zahlen[i] = Convert.ToInt32(block);
            }

            return zahlen;
        }
    }
}
