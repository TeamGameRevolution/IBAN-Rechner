﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static int ZaehleBuchstaben(string text, char buchstabe)
        {
            int zaehler = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == buchstabe)
                {
                    zaehler++;
                }
            }

            return zaehler;
        }
    }
}
