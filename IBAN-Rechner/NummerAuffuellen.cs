﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			NummerAuffuellen.cs
Einsatz:		Füllt die angegebene Nummer auf X Stellen mit vorangehenden Nullen auf
Beschreibung:	Fügt Nullen an den Anfang der angegebenen Nummer an
Funktionen:		KontonummerAuffuellen
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static string NummerAuffuellen(string nummer, int anzahl)
        {
            int anzahlNullen = anzahl - nummer.Length;
            for (int i = 0; i < anzahlNullen; i++)
            {
                nummer = "0" + nummer;
            }

            return nummer;
        }
    }
}
