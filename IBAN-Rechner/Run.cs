﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			Run.cs
Einsatz:		Programmablauf
Beschreibung:	Enthält die Logik des Hauptmenues
Funktionen:		Run
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static void Run(IBANFormat[] formate, DeutscheBankleitzahl[] bankleitzahlen)
        {
            bool beenden = false;
            while (!beenden)
            {
                Console.Clear();
                Splash();
                Console.WriteLine();
                Console.WriteLine("1) IBAN berechnen");
                Console.WriteLine("2) IBAN validieren");
                Console.WriteLine();
                Console.WriteLine("x) Beenden");
                Console.WriteLine();
                Console.Write("Ihre Eingabe: ");
                string eingabe = Console.ReadLine();

                switch (eingabe)
                {
                    case "1":
                    {
                        Console.Clear();
                        Console.Write("Ländercode: ");
                        string laendercode = Console.ReadLine();
                        if (laendercode.Length != 2 || (laendercode[0] < 65 || laendercode[0] > 90) || (laendercode[1] < 65 || laendercode[1] > 90))
                        {
                            Console.WriteLine("Invalider Ländercode! Drücken Sie Enter zum Fortfahren!");
                            Console.ReadLine();
                            break;
                        }
                        AbfrageKontodaten(laendercode, formate, bankleitzahlen);
                        break;
                    }
                    case "2":
                    {
                        Console.Clear();
                        Console.Write("IBAN: ");
                        string iban = Console.ReadLine();
                        string grund = "";
                        bool ibanValide = IBANValidieren(iban, ref grund, formate);
                        if (!ibanValide)
                        {
                            Console.WriteLine("Invalide IBAN! Grund: " + grund);
                            Console.WriteLine("Drücken Sie Enter zum Fortfahren!");
                            Console.ReadLine();
                            break;
                        }
                        Console.WriteLine("Die eingegebene IBAN ist valide! Drücken Sie Enter zum Fortfahren!");
                        Console.ReadLine();
                        break;
                    }
                    case "x":
                    {
                        beenden = true;
                        break;
                    }
                }
            }
        }
    }
}
